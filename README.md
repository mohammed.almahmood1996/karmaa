# steps

## 1-

composer i

## 2-

php artisan migrate

## 3-

if you want ,you could add the option --seed to seed the database with 100 000 user and images

## 4- php artisan serve

to see the datatable "I didn't use any pagination to show the tabledata by scrool"

### Notes:

1- I have used query builder instead of ORM because in Some Places , query builder is faster. (done )

2-i have Made the number of users being returned instead of 5 customizable from the API
endpoint. (done )

3- i have Used the API to render a simple HTML table that shows the user ranking (done )

4- Always show 5 users, if the requested user has the position 1, show him first, and show
the next 4 lower users in ranking, also if the user is the lowest in ranking, show the user
at the end of the list and the next 4 users are higher than him in the ranking.  (done )
