<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Users Karma</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Styles --> --}}
    <script src="https://cdn.tailwindcss.com"></script>

    <style>

    </style>

    <style>
        .container {
            padding: 2rem 0rem;
        }

        h4 {
            margin: 2rem 0rem 1rem;
        }

        .table-image {

            td,
            th {
                vertical-align: middle;
            }
        }

    </style>
</head>

<body>

    <div class="container  flex justify-content-center align-content-center flex-col">
        <table class="table-fixed  border-2 border-gray-300 border-rounded-1  m-24">
            <thead class="   p-4">
                <tr >
                    <th>#</th>
                    <th>Naam</th>
                    <th>score</th>


                </tr>
            </thead>
            <tbody>

                @foreach ($users as $key => $user)
                    <tr>
                        <td align="center">{{ $user->position }}</td>
                        <td align="center">
                            <div class=" my-4">
                                <img src="{{ $user->image }}" class="rounded-full">
                                <p class="font-bold text-gray-500">
                                    {{ $user->username }}
                                </p>

                            </div>

                        </td>

                        <td class="w-100" align="center"> {{ $user->karma_score }}</td>

                    </tr>
                @endforeach

            </tbody>


        </table>
        <div class="flex justify-content-center align-content-center mx-24">
            {{-- {{ $users->links() }} --}}
        </div>

    </div>


</body>

</html>
