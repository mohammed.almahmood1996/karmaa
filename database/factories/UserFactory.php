<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Image;
use App\Models\User;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => $this->faker->userName.$this->faker->numberBetween(0, 1000).$this->faker->numberBetween(0, 1000),
            'karma_score' => $this->faker->numberBetween(0, 100000),
            'image_id' => Image::factory(),
        ];
    }
}
