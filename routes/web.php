<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $users =   DB::table('users')->join('images', 'users.image_id', '=', 'images.id')
        ->select('users.id', 'users.username', 'users.karma_score', 'images.url as image')
        ->orderBy('karma_score', 'desc')
        ->get('5');


    $users->map(function ($user, $key) {
        $user->position = $key + 1;
        return $user;
    });

    return view('welcome', ['users' => $users]);
});
