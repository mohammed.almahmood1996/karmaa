<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'karma_score',
        'image_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'image_id' => 'integer',
    ];

    public function image()
    {
        return $this->belongsTo(Image::class);
    }
    public function scopeOrderedByKarma($query)
    {
        $users = DB::table('users')->join('images', 'users.image_id', '=', 'images.id')
            ->select('users.id', 'users.username', 'users.karma_score', 'images.url as image')
            ->orderBy('karma_score', 'desc')
            ->get();
        $users->map(function ($user, $key) {
            $user->position = $key + 1;
            return $user;
        });
        return $users;
    }
}
