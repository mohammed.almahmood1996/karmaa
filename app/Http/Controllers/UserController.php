<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    public $pos = 0;
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\UserCollection
     */
    public function index(Request $request)
    {
        $users = User::all();

        return new UserCollection($users);
    }

    /**
     * @param \App\Http\Requests\UserStoreRequest $request
     * @return \App\Http\Resources\UserResource
     */
    public function store(UserStoreRequest $request)
    {
        $user = User::create($request->validated());

        return new UserResource($user);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \App\Http\Resources\UserResource
     */
    public function show(Request $request, User $user)
    {
        return new UserResource($user);
    }

    /**
     * @param \App\Http\Requests\UserUpdateRequest $request
     * @param \App\Models\User $user
     * @return \App\Http\Resources\UserResource
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $user->update($request->validated());

        return new UserResource($user);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return response()->noContent();
    }
    public function getUserKarmaPosition($id, $numberOfRequestedUsers = 5)
    {

        $user = User::findOrFail($id);
        if ($numberOfRequestedUsers == 0) {

            return response()->noContent();
        }
        if ($numberOfRequestedUsers == 1) {
            return $user;
        }

        $CountOfUsers = DB::table('users')->count();
        $users = User::orderedByKarma();
        $pos = $this->getSelectedUSerPosition($users, $id);
        $UsersCollection = $this->calulateUserOverAllPosition($users, $numberOfRequestedUsers, $pos, $CountOfUsers);
        return $UsersCollection;
    }

    public function getSelectedUSerPosition($users, $id)
    {
        $users->map(function ($user, $key) use ($id) {

            if ($user->id == $id) {

                $this->pos = $key;
            }
            return $user;
        });
        $pos = $this->pos;

        return    $pos;
    }
    public function calulateUserOverAllPosition($users, $numberOfRequestedUsers, $pos, $CountOfUsers)
    {
        $UsersCollection = [];
        $x = $numberOfRequestedUsers % 2 == 0 ? floor($numberOfRequestedUsers / 2) - 1 : floor($numberOfRequestedUsers / 2);
        $y = ceil($numberOfRequestedUsers / 2);

        $isFirstUser = ($pos == 0 ? true : false);
        $isSecondtUser = ($pos == 1 ? true : false);
        $isBeforLastUser = ($pos == $CountOfUsers - 2 ? true : false);
        $isLastUser = ($pos == $CountOfUsers - 1 ? true : false);

        if ($pos > 0 && $pos < $CountOfUsers - 1) {
            if ($isSecondtUser || $isBeforLastUser) {
                for ($i = 0; $i < $numberOfRequestedUsers; $i++) {
                    $isSecondtUser ?  $UsersCollection[] = $users[$pos - 1 + $i] : $UsersCollection[] = $users[$pos + 1 - $i];
                }
                $UsersCollection = collect($UsersCollection)->sortBy('position');
                $UsersCollection = $UsersCollection->values()->all();
            } else {
                for ($i = -$x; $i < $y; $i++) {
                    $UsersCollection[] = $users[$pos + $i];
                }
            }
        }
        if ($isFirstUser || $isLastUser) {
            for ($i = 0; $i < $numberOfRequestedUsers; $i++) {
                $isFirstUser ?  $UsersCollection[] = $users[$pos + $i] : $UsersCollection[] = $users[$pos - $i];
            }
        }
        return   $UsersCollection;
    }
}
