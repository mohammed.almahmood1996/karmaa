<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageStoreRequest;
use App\Http\Requests\ImageUpdateRequest;
use App\Http\Resources\ImageCollection;
use App\Http\Resources\ImageResource;
use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\ImageCollection
     */
    public function index(Request $request)
    {
        $images = Image::all();

        return new ImageCollection($images);
    }

    /**
     * @param \App\Http\Requests\ImageStoreRequest $request
     * @return \App\Http\Resources\ImageResource
     */
    public function store(ImageStoreRequest $request)
    {
        $image = Image::create($request->validated());

        return new ImageResource($image);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Image $image
     * @return \App\Http\Resources\ImageResource
     */
    public function show(Request $request, Image $image)
    {
        return new ImageResource($image);
    }

    /**
     * @param \App\Http\Requests\ImageUpdateRequest $request
     * @param \App\Models\Image $image
     * @return \App\Http\Resources\ImageResource
     */
    public function update(ImageUpdateRequest $request, Image $image)
    {
        $image->update($request->validated());

        return new ImageResource($image);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Image $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Image $image)
    {
        $image->delete();

        return response()->noContent();
    }
}
